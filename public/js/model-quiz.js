var dispAnswer = sessionStorage.getItem("dispAnswer");
var randAnswer = sessionStorage.getItem("randAnswer");
var randQuestion = sessionStorage.getItem("randQuestion");
const jsonContent = JSON.parse(sessionStorage.getItem("jsonContent"));
var jsonQuestions = jsonContent.questions;
var submittedQuestions = JSON.parse((sessionStorage.getItem("jsonSubmittedQuestions") === null) ? '[]' : sessionStorage.getItem("jsonSubmittedQuestions"));




function main(){
	let tempQuestions = jsonQuestions;
	
	for(let n=0; n<tempQuestions.length; n++){
		let tempQuizList = document.getElementById("quizList");
		let tempAnswers = tempQuestions[n].answers;
		let tempQuestionDesc = tempQuestions[n].questionDesc;
		let tempQuestionImg = tempQuestions[n].questionImg;
		let tempQuestionType = tempQuestions[n].questionType;
		let tempExpl = tempQuestions[n].explanation;
		var quizItem = document.createElement("li");
		
		quizItem.className = "quizItem d-none list-group-item";
		quizItem.innerHTML = `
			<div class="row" id="indQuestion${n+1}">
				<div class="col-md-7 col-sm-12">
					<h5>${tempQuestionDesc}</h5>
					<h6 id="questionType${n+1}">${tempQuestionType}</h6>
					<div class="col-md-12 col-sm-12" id="questionImg${n+1}">
						
					</div>
				</div>
				<div class="col-md-5 col-sm-12" id="ansGrp${n+1}"> <!--Answer Group-->
					<ul class="list-group">
						
					</ul>
				</div>
				<div class="col-md-12 col-sm-12 d-none" id="expl${n+1}">
					<h6>Explanation: ${tempExpl}</h6>
				</div>
			</div>
			
		`;
		
		
		tempQuizList.appendChild(quizItem);
		if ((tempQuestionImg)&&(tempQuestionImg != "")){
			var quizImg = document.createElement("img");
			let tempQuizItem = document.getElementById(`questionImg${n+1}`);
			quizImg.className = "img-fluid";
			quizImg.src = `${tempQuestionImg}`;
			tempQuizItem.appendChild(quizImg);
		}
		
		
		
		for(let i=0; i<tempQuestions[n].answers.length; i++){
			let tempAnsGrp = document.getElementById(`ansGrp${n+1}`);
			let tempUl = tempAnsGrp.getElementsByTagName("UL")[0];
			let tempAnswersItemsDesc = tempQuestions[n].answers[i].answerDesc;
			let tempAnswersItemsImg = tempQuestions[n].answers[i].answerImg;
			let tempAnswersItemsIsCorrect = tempQuestions[n].answers[i].isCorrectAns;
			let tempAnswersItemIdentification = tempQuestions[n].answers[0].answerDesc;
			let displayAns = "";
	
			//console.log(tempAnswersItemsDesc);
			
			//var ansChoice = document.createElement("li");
			//ansChoice.className = "list-group-item";
			var ansDiv = document.createElement("div");
			ansDiv.className = "border border-dark rounded p-3 mb-2 col-md-12 col-sm-12 mt-2";
			
			switch(tempQuestionType){
				case 'multipleChoice-single':
					ansDiv.innerHTML = `
						<li class="list-group-item" onclick=changeInputVal('${n+1}','ansGrp${n+1}-indAns${i+1}')>
							<input class="form-check-input" name="ansGrp${n+1}" type="radio" value="" onclick="this.checked = false;" id="ansGrp${n+1}-indAns${i+1}" readonly="readonly">
							<label class="form-check-label" for="ansGrp${n+1}-indAns${i+1}-disable" onclick="this.checked = false;">
								${tempAnswersItemsDesc}
							</label>
						</li>
						
					`;
					if ((tempAnswersItemsImg)&&(tempAnswersItemsImg != "")){
						var tempAnsImg = document.createElement("img");
						let tempAnsLi = ansDiv.getElementsByTagName("li")[0];
						tempAnsImg.className = "img-fluid";
						tempAnsImg.src = `${tempAnswersItemsImg}`;
						tempAnsLi.appendChild(tempAnsImg);
					}
					break;
				case 'multipleChoice-multi':
					ansDiv.innerHTML = ansDiv.innerHTML = `
						<li class="list-group-item" onclick="changeInputVal('${n+1}','ansGrp${n+1}-indAns${i+1}')">
							<input class="form-check-input" name="ansGrp${n+1}" type="checkbox" onclick="${displayAns}" id="ansGrp${n+1}-indAns${i+1}">
							<label class="form-check-label" for="ansGrp${n+1}-indAns${i+1}-disable" onclick="this.checked = false;">
								${tempAnswersItemsDesc}
							</label>
						</li>
					`;
					if ((tempAnswersItemsImg)&&(tempAnswersItemsImg != "")){
						var tempAnsImg = document.createElement("img");
						let tempAnsLi = ansDiv.getElementsByTagName("li")[0];
						tempAnsImg.className = "img-fluid";
						tempAnsImg.src = `${tempAnswersItemsImg}`;
						tempAnsLi.appendChild(tempAnsImg);
					}
					break;
				case 'identification':
					ansDiv.innerHTML = `
						<li class="list-group-item">
							<label class="form-check-label" for="ansGrp${n+1}-indAns${i+1}">
								Your Answer:
							</label>
							<input class="form-control" name="ansGrp${n+1}" type="text" value="" onblur="saveAns('${n+1}','ansGrp${n+1}-indAns${i+1}')" id="ansGrp${n+1}-indAns${i+1}">
							<label class="d-none" id="ansGrp${n+1}-desc"></label>
						</li>
					`;
					break;
				
			}
			
			tempUl.appendChild(ansDiv);
			//tempUl.appendChild(ansChoice);
			//ansChoice.appendChild(ansDiv);
			
			
			if (dispAnswer == 'true'){
				//highlight correct answer
				if ((tempQuestionType == 'multipleChoice-single')||(tempQuestionType == 'multipleChoice-multi')){
					if (tempAnswersItemsIsCorrect){
						document.getElementById(`ansGrp${n+1}-indAns${i+1}`).parentNode.classList.add('ansIsCorrect');
					} else {
						document.getElementById(`ansGrp${n+1}-indAns${i+1}`).parentNode.classList.add('ansIsWrong');
					}
				}
			}
		}
	}
	
}


function pageNav(){
	let pageCount = jsonContent.questions.length;
	let pageNavId = document.getElementById("pageNav");
	
	
	var pagePrev = document.createElement("li");
	pagePrev.className = "page-item";
	pagePrev.innerHTML = `
		<a class="page-link" href="#" id="quizPagePrev">Previous</a>
	`;
	
	var pageNext = document.createElement("li");
	pageNext.className = "page-item";
	pageNext.innerHTML = `
		<a class="page-link" href="#" id="quizPageNext">Next</a>
	`;
	
	pageNavId.appendChild(pagePrev);
	
	for(let k=1; k <= pageCount; k++ ){
		var pageItem = document.createElement("li");
		var isMarked = sessionStorage.getItem(`markedQuestion${k}`)||"false";
		pageItem.className = "page-item";
		if (isMarked == "true"){
			pageItem.innerHTML = `
				<a class="page-link bg-warning" href="?page=${k}" id="quizPageItem${k}">${k}</a>
			`;
		} else {
			if (submittedQuestions.includes(k)){
				pageItem.innerHTML = `
				<a class="page-link text-bg-success" href="?page=${k}" id="quizPageItem${k}">${k}</a>
			`;
			} else {
				pageItem.innerHTML = `
				<a class="page-link" href="?page=${k}" id="quizPageItem${k}">${k}</a>
			`;
			}
			
		}
		
	pageNavId.appendChild(pageItem);
	}
	
	pageNavId.appendChild(pageNext);
}

function saveAns(grp,ans){
	//console.log(grp+","+ans);
	let tempSubmittedAns = sessionStorage.getItem("jsonSubmittedAns");
	let SubmittedAnsContent = JSON.parse(tempSubmittedAns);
	let questionType = document.getElementById(`questionType${grp}`).textContent;
	let submitButton = document.getElementById("submitBtn");
	let tempSubmittedQuestions = submittedQuestions;
	
	//console.log(SubmittedAnsContent);
	if ((questionType == "multipleChoice-single")||(questionType == "multipleChoice-multi")){
		//console.log(questionType);
		
		if (!(SubmittedAnsContent[`ansGrp${grp}`].includes(ans))&&(questionType == "multipleChoice-single")){
			SubmittedAnsContent[`ansGrp${grp}`] = [];
			SubmittedAnsContent[`ansGrp${grp}`].push(ans);
			//console.log("add "+ ans);
			//console.log(SubmittedAnsContent);
			sessionStorage.setItem("jsonSubmittedAns", JSON.stringify(SubmittedAnsContent));
		}
		else if (!(SubmittedAnsContent[`ansGrp${grp}`].includes(ans))&&(questionType == "multipleChoice-multi")){
			SubmittedAnsContent[`ansGrp${grp}`].push(ans);
			//console.log("add "+ ans);
			//console.log(SubmittedAnsContent);
			sessionStorage.setItem("jsonSubmittedAns", JSON.stringify(SubmittedAnsContent));
		}
		else {
			removeA(SubmittedAnsContent[`ansGrp${grp}`], ans);
			//console.log("remove "+ ans);
			//console.log(SubmittedAnsContent);
			sessionStorage.setItem("jsonSubmittedAns", JSON.stringify(SubmittedAnsContent));
		}
	}
	else { //identification
		let tempAns = document.getElementById(`${ans}`);
		let tempAnsValue = tempAns.value;
		let tempAnswersItemsDesc = jsonQuestions[parseInt(grp)-1].answers[0].answerDesc;
		
		if(tempAns && tempAnsValue){ //check if not empty
			tempAns.parentNode.className = "border border-dark rounded p-3 mb-2 col-md-12 col-sm-12 mt-2";
			
			if (tempAnswersItemsDesc == tempAnsValue){
				tempAns.parentNode.classList.add('ansIsCorrect');
			} else{
				tempAns.parentNode.classList.add('ansIsWrong');
				document.getElementById(`ansGrp${grp}-desc`).innerHTML = `Correct Answer: <b>${tempAnswersItemsDesc}</b>`;
			}
			SubmittedAnsContent[`ansGrp${grp}`] = [];
			SubmittedAnsContent[`ansGrp${grp}`].push(tempAnsValue);
			sessionStorage.setItem("jsonSubmittedAns", JSON.stringify(SubmittedAnsContent));
		} else {
			SubmittedAnsContent[`ansGrp${grp}`] = [];
			sessionStorage.setItem("jsonSubmittedAns", JSON.stringify(SubmittedAnsContent));
		}
	}
	
	document.getElementById(`quizPageItem${grp}`).classList.remove("text-bg-success");
	removeA(tempSubmittedQuestions, parseInt(grp));
	sessionStorage.setItem("jsonSubmittedQuestions", JSON.stringify(tempSubmittedQuestions));
	console.log(SubmittedAnsContent);
	
	if (SubmittedAnsContent[`ansGrp${grp}`].length > 0){
		submitButton.className = "btn btn-primary";
	} else {
		submitButton.className = "btn btn-primary disabled";
	}
	//console.log("ans selected");

}

function changeInputVal(grp,ans){
	let tempAns = document.getElementById(ans);
	let questionType = document.getElementById(`questionType${grp}`).textContent;
	
	if((tempAns.checked)&&(questionType == 'multipleChoice-multi')){
		tempAns.checked = false;
		saveAns(grp,ans);
	} else if (!tempAns.checked){
		tempAns.checked = true;
		saveAns(grp,ans);
	}
	//else do nothing
	//console.log(document.getElementById(ans).checked);
}

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


main();
pageNav();
