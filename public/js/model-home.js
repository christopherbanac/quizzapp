var fr = new FileReader();

function submitJson() {
	sessionStorage.setItem("dispAnswer", document.getElementById("dispAnswer").checked);
	sessionStorage.setItem("randAnswer", document.getElementById("randAnswer").checked);
	sessionStorage.setItem("randQuestion", document.getElementById("randQuestion").checked);
	
	var dispAnswer = sessionStorage.getItem("dispAnswer");
	var randAnswer = sessionStorage.getItem("randAnswer");
	var randQuestion = sessionStorage.getItem("randQuestion");
	const jsonContent = JSON.parse(JSON.parse(sessionStorage.getItem("jsonContent")));
	var jsonQuestions = jsonContent.questions;
	let tempQuestions = jsonQuestions;
	
	//console.log("jsonQuestions before: " + jsonQuestions);
	
	if (randQuestion == 'true'){
		jsonQuestions = shuffle(tempQuestions);	
		//console.log(tempQuestions);
	}
	
	if (randAnswer == 'true'){
		console.log(tempQuestions);
		//tempQuestions.forEach(shuffleAns);
		for(let n=0; n<tempQuestions.length; n++){
			let jsonAns = tempQuestions[n].answers;
			tempQuestions.answers = shuffle(jsonAns);
			console.log(tempQuestions);
		}
	}
	sessionStorage.setItem("jsonContent",  JSON.stringify(jsonContent));
/* 	console.log(JSON.parse(fr.result));
	console.log(fr.result);
	console.log(jsonContent);
	console.log(JSON.parse(jsonContent)); */
	//console.log(jsonContent);
	//console.log(dispAnswer);
	//console.log(randAnswer);
	//console.log(randQuestion);
}

function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}