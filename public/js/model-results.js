var dispAnswer = sessionStorage.getItem("dispAnswer");
var randAnswer = sessionStorage.getItem("randAnswer");
var randQuestion = sessionStorage.getItem("randQuestion");
const jsonContent = JSON.parse(sessionStorage.getItem("jsonContent"));
var jsonQuestions = jsonContent.questions;
var submittedQuestions = JSON.parse((sessionStorage.getItem("jsonSubmittedQuestions") === null) ? '[]' : sessionStorage.getItem("jsonSubmittedQuestions"));
var tempSubmittedAns = sessionStorage.getItem("jsonSubmittedAns");
var score = 0;
	



function main(){
	let tempQuestions = jsonQuestions;
	let SubmittedAnsContent = JSON.parse((tempSubmittedAns === null) ? '{}' : tempSubmittedAns);
	
	for(let n=0; n<tempQuestions.length; n++){
		let tempQuizList = document.getElementById("quizList");
		let tempAnswers = tempQuestions[n].answers;
		let tempQuestionDesc = tempQuestions[n].questionDesc;
		let tempQuestionImg = tempQuestions[n].questionImg;
		let tempQuestionType = tempQuestions[n].questionType;
		let tempExpl = tempQuestions[n].explanation;
		let tempTotalCorrect = tempAnswers.filter(function(item){return item.isCorrectAns==true}).length;
		let tempCurrentCorrect = 0;
		var quizItem = document.createElement("li");
		
		quizItem.className = "quizItem d-block list-group-item";
		quizItem.innerHTML = `
			<div class="row" id="indQuestion${n+1}">
				<div class="col-md-7 col-sm-12">
					<h5>${tempQuestionDesc}</h5>
					<h6 id="questionType${n+1}">${tempQuestionType}</h6>
					<div class="col-md-12 col-sm-12" id="questionImg${n+1}">
						
					</div>
				</div>
				<div class="col-md-5 col-sm-12" id="ansGrp${n+1}"> <!--Answer Group-->
					<ul class="list-group">
						
					</ul>
				</div>
				<div class="col-md-12 col-sm-12 d-block" id="expl${n+1}">
					<h6>Explanation: ${tempExpl}</h6>
				</div>
			</div>
			
		`;
		tempQuizList.appendChild(quizItem);
		if ((tempQuestionImg)&&(tempQuestionImg != "")){
			var quizImg = document.createElement("img");
			let tempQuizItem = document.getElementById(`questionImg${n+1}`);
			quizImg.className = "img-fluid";
			quizImg.src = `${tempQuestionImg}`;
			tempQuizItem.appendChild(quizImg);
		}
		
		for(let i=0; i<tempQuestions[n].answers.length; i++){
			let tempAnsGrp = document.getElementById(`ansGrp${n+1}`);
			let tempUl = tempAnsGrp.getElementsByTagName("UL")[0];
			let tempAnswersItemsDesc = tempQuestions[n].answers[i].answerDesc;
			let tempAnswersItemsImg = tempQuestions[n].answers[i].answerImg;
			let tempAnswersItemsIsCorrect = tempQuestions[n].answers[i].isCorrectAns;
			let tempAnswersItemIdentification = tempQuestions[n].answers[0].answerDesc;
			let displayAns = "";
	
			//console.log(tempAnswersItemsDesc);
			
			var ansChoice = document.createElement("li");
			ansChoice.className = "list-group-item";
			var ansDiv = document.createElement("div");
			ansDiv.className = "border border-dark rounded p-3 mb-2 col-md-12 col-sm-12 mt-2";
			
			switch(tempQuestionType){
				case 'multipleChoice-single':
					ansDiv.innerHTML = `
						<input class="form-check-input" name="ansGrp${n+1}" type="radio" value="" id="ansGrp${n+1}-indAns${i+1}" disabled>
						<label class="form-check-label" for="ansGrp${n+1}-indAns${i+1}">
							${tempAnswersItemsDesc}
						</label>
					`;
					if ((tempAnswersItemsImg)&&(tempAnswersItemsImg != "")){
						var tempAnsImg = document.createElement("img");
						tempAnsImg.className = "img-fluid";
						tempAnsImg.src = `${tempAnswersItemsImg}`;
						ansDiv.appendChild(tempAnsImg);
					}
					break;
				case 'multipleChoice-multi':
					ansDiv.innerHTML = ansDiv.innerHTML = `
						<input class="form-check-input" name="ansGrp${n+1}" type="checkbox" id="ansGrp${n+1}-indAns${i+1}" disabled>
						<label class="form-check-label" for="ansGrp${n+1}-indAns${i+1}">
							${tempAnswersItemsDesc}
						</label>
					`;
					if ((tempAnswersItemsImg)&&(tempAnswersItemsImg != "")){
						var tempAnsImg = document.createElement("img");
						tempAnsImg.className = "img-fluid";
						tempAnsImg.src = `${tempAnswersItemsImg}`;
						ansDiv.appendChild(tempAnsImg);
					}
					break;
				case 'identification':
					ansDiv.innerHTML = `
						<label class="form-check-label" for="ansGrp${n+1}-indAns${i+1}">
							Your Answer:
						</label>
						<input class="form-control" name="ansGrp${n+1}" type="text" value="" id="ansGrp${n+1}-indAns${i+1}" disabled>
						<label>
							Correct Answer: <b>${tempAnswersItemIdentification}</b>
						</label>
					`;
					break;
			}
			
			tempUl.appendChild(ansChoice);
			ansChoice.appendChild(ansDiv);
			
			//check selected answers
			
			//console.log(JSON.parse(tempSubmittedAns));
			let tempChoice = document.getElementById(`ansGrp${n+1}-indAns${i+1}`);
			if (JSON.parse(tempSubmittedAns)[`ansGrp${n+1}`].includes(`ansGrp${n+1}-indAns${i+1}`)){
				tempChoice.checked = true;
				
			}
			
			
			//displayAns
			let indAns = document.getElementById(`ansGrp${n+1}-indAns${i+1}`);
			switch(tempQuestionType){
				case 'multipleChoice-single':
					if (tempAnswersItemsIsCorrect){
						indAns.parentNode.classList.add('text-bg-success');
						if(indAns.checked){
							score++;
						}
					} else { //tempAnswersItemsIsCorrect == false
						if (tempChoice.checked){
							indAns.parentNode.classList.add('text-bg-danger');
						}
					}
					break;
				case 'multipleChoice-multi':
					if (tempAnswersItemsIsCorrect){
						indAns.parentNode.classList.add('text-bg-success');
						if(indAns.checked){
							tempCurrentCorrect++;
						}
					} else { //tempAnswersItemsIsCorrect == false
						if (tempChoice.checked){
							indAns.parentNode.classList.add('text-bg-danger');
							tempCurrentCorrect--;
						}
					}
					break;
				case 'identification':
					let tempSubAns = JSON.parse(tempSubmittedAns)[`ansGrp${n+1}`][0];
					//console.log("tempAnswersItemIdentification: "+tempAnswersItemIdentification);
					tempChoice.value = tempSubAns;
					if (tempSubAns.toUpperCase() === tempAnswersItemIdentification.toUpperCase()){
						indAns.parentNode.classList.add('text-bg-success');
						//tempCurrentCorrect++;
						score++;
					} else {
						indAns.parentNode.classList.add('text-bg-danger');
						//tempCurrentCorrect--;
					}
					break;
				default:
					//do nothing... for now
			}
		}
		if ((tempTotalCorrect == tempCurrentCorrect)&&(tempQuestionType === 'multipleChoice-multi')){
			score++;
		}
		tempCurrentCorrect = 0;
	}
	
	document.getElementById("score").textContent = `Your score is: ${score}/${tempQuestions.length}`;
	
}

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

main();
