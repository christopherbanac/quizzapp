window.onload = onPageLoad();
function onPageLoad() {
	
	const params = new Proxy(new URLSearchParams(window.location.search), {
		get: (searchParams, prop) => searchParams.get(prop),
	});
	var pageValue = params.page;
	
	//initialize jsonSubmittedAns
	var tempSubmittedAns = sessionStorage.getItem("jsonSubmittedAns");
	//console.log(tempSubmittedAns);
	let tempQuestionType = document.getElementById(`questionType${pageValue}`).textContent;
	let SubmittedAnsContent = JSON.parse((tempSubmittedAns === null) ? '{}' : tempSubmittedAns);
	if (!(`ansGrp${pageValue}` in SubmittedAnsContent)){ //if ansGrp key of current page does not exists yet in submittedAnswers
		SubmittedAnsContent[`ansGrp${pageValue}`] = [];
		sessionStorage.setItem("jsonSubmittedAns", JSON.stringify(SubmittedAnsContent));
		//console.log(SubmittedAnsContent);
	} 
	else {
		let submitAnsTemp = SubmittedAnsContent[`ansGrp${pageValue}`];
		//console.log(`${submitAnsTemp.length}`);
		//console.log(`${document.getElementById(`${submitAnsTemp[0]}`)}`);
		for(let i=1; i<=submitAnsTemp.length; i++){
			if (tempQuestionType != "identification"){
				document.getElementById(`${submitAnsTemp[i-1]}`).checked = true;
			} else {
				//console.log(`${submitAnsTemp[i-1]}`);
				//console.log(`ansGrp${pageValue}`);
				document.getElementById(`ansGrp${pageValue}-indAns1`).value = `${submitAnsTemp[i-1]}`;
				//document.getElementById(`ansGrp${pageValue}`).setAttribute('value',`${submitAnsTemp[i-1]}`);
			}
		}
	}
	
	if (SubmittedAnsContent[`ansGrp${pageValue}`].length > 0){
		document.getElementById("submitBtn").className = "btn btn-primary";
	}
	
	//displayAnswer
	if (dispAnswer === 'true'){
		document.getElementById("submitBtn").textContent="Reveal Correct Answers";
	}
	document.getElementById("submitBtn").setAttribute('onclick',`submitAns(${pageValue})`);

	//page view
	let dispQuestion = document.getElementById(`indQuestion${pageValue}`);
	dispQuestion.parentNode.className = "quizItem d-block";
	
	//console.log(pageValue);
	//console.log(`indQuestion${pageValue}`);
	
	//nav pagination
	let activePage = document.getElementById(`quizPageItem${pageValue}`);
	activePage.parentNode.className = "page-item active";
	//activePage.className = "page-link disabled";
	
	//console.log(pageValue);
	//console.log(`quizPageItem${pageValue}`);
	
	let prevPage = document.getElementById("quizPagePrev");
	if (pageValue == 1){
		prevPage.parentNode.className = "page-item disabled";
	}
	else {
		prevPage.href = `?page=${pageValue-1}`;
	}
	
	let nextPage = document.getElementById("quizPageNext");
	if (pageValue == jsonContent.questions.length){
		nextPage.parentNode.className = "page-item disabled";
	}
	else {
		nextPage.href = `?page=${parseInt(pageValue)+1}`;
	}
	
	//markedQuestions
	let isChecked = sessionStorage.getItem(`markedQuestion${pageValue}`);
	let markedQuestionContainer = document.getElementById("markedQuestion");
	
	if(isChecked == "true"){
		markedQuestionContainer.innerHTML = `
			<input class="form-check-input" type="checkbox" onchange="saveMark('markedQuestion${pageValue}')" id="markedQuestion${pageValue}" checked>
			<label class="form-check-label" for="markedQuestion${pageValue}">
				Mark Question for later
			</label>
		`
	}else{
		markedQuestionContainer.innerHTML = `
			<input class="form-check-input" type="checkbox" onchange="saveMark('markedQuestion${pageValue}')" id="markedQuestion${pageValue}">
			<label class="form-check-label" for="markedQuestion${pageValue}">
				Mark Question for later
			</label>
		`
	}
	
}

function saveMark(checkboxId){
	//console.log(checkboxId);
	let tempMark = document.getElementById(checkboxId);
	sessionStorage.setItem(checkboxId, tempMark.checked);
}

function saveSubmit(pageValue){
	let tempMark = document.getElementById(`markedQuestion${pageValue}`);
	let tempSubmittedQuestions = submittedQuestions;
	//console.log(tempMark.checked);
	//console.log(parseInt(pageValue));
	
	
	if (tempMark.checked == true){
		removeA(tempSubmittedQuestions, pageValue);
	}else{
		if (!tempSubmittedQuestions.includes(pageValue)){
			tempSubmittedQuestions.push(pageValue);
		}
	}
	tempSubmittedQuestions.toSorted((a, b) => a - b);
	sessionStorage.setItem("jsonSubmittedQuestions", JSON.stringify(tempSubmittedQuestions));
}

function submitAns(pageValue){
	let submitBtn = document.getElementById("submitBtn");
	let correctAns = document.getElementsByClassName("ansIsCorrect");
	let wrongAns = document.getElementsByClassName("ansIsWrong");
	
	//console.log("before: " +submittedQuestions);
	if(submitBtn.textContent == 'Reveal Correct Answers'){
		if(document.getElementById(`ansGrp${pageValue}-desc`)){
			document.getElementById(`ansGrp${pageValue}-desc`).className = "d-block";
		}
		for(var i = 0, all = correctAns.length; i < all; i++){   
			correctAns[i].classList.add('text-bg-success');
		}
		for(var i = 0, all = wrongAns.length; i < all; i++){   
			wrongAns[i].classList.add('text-bg-danger');
		}
		document.getElementById("submitBtn").textContent="Submit Answer";
		document.getElementById(`expl${pageValue}`).classList="col-md-12 col-sm-12 d-block";
	} else {
		saveSubmit(pageValue);
		if (submittedQuestions.length == jsonContent.questions.length){
			const seeScore = new bootstrap.Modal(document.getElementById('seeScoreModal'), 'backdrop')
			seeScore.show();
		}
		else if (pageValue == jsonContent.questions.length){
			submitBtn.href = `?page=1`;
		}
		else {
			submitBtn.href = `?page=${parseInt(pageValue)+1}`;
		}	
	}
	//console.log(submittedQuestions);
	
	
}
